Gather town rooms
=================


This repository contains some rooms I've built for [Gather
Town](https://gather.town). They mostly use Gather Town tiles, hence the
submodule.


Prerequisites
=============

These are created using [Tiled](https://www.mapeditor.org/), so you'll need
that installed.


Installing
==========

This repository uses the Gather Town tiles, so you'll need to initialise those
properly too. First, clone this repository

```
git clone https://gitlab.com/wpettersson/gather-rooms.git
```

Then, to install the Gather Town tiles appropriately, first `cd gather-rooms` and then run
```
git submodule update --init
```
